<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\NewsApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

// Public Routes

// Route::get('/test', [UserController::class, 'testing']);
Route::get('/test', [NewsApiController::class, 'testing']);

Route::middleware(['auth:sanctum'])->group(function(){
    Route::get('/newsapi', [NewsApiController::class, 'getNewsApiData']);
});

// Route::post('/send-reset-password-email', [PasswordResetController::class, 'send_reset_password_email']);


// Protected Routes

Route::prefix('user')->group(function(){
    Route::post('/register', [UserController::class, 'register']);
    Route::post('/login', [UserController::class, 'login']);

    Route::middleware(['auth:sanctum'])->group(function(){
        Route::post('/logout', [UserController::class, 'logout']);
        Route::get('/loggeduser', [UserController::class, 'logged_user']);
        Route::post('/changepassword', [UserController::class, 'change_password']);
        // Route::get('/newsapi', [NewsApiController::class, 'getNewsApiData']);

    });
});
