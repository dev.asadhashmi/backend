-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: mysql
-- Generation Time: Jun 30, 2023 at 03:45 PM
-- Server version: 8.0.32
-- PHP Version: 8.1.20

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `backend`
--

-- --------------------------------------------------------

--
-- Table structure for table `apiLogs`
--

CREATE TABLE `apiLogs` (
  `id` int NOT NULL,
  `apiName` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `apiEndPoint` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `responceCode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `responceJson` text CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `addedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_reset_tokens_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reset_tokens`
--

CREATE TABLE `password_reset_tokens` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `expires_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `personal_access_tokens`
--

INSERT INTO `personal_access_tokens` (`id`, `tokenable_type`, `tokenable_id`, `name`, `token`, `abilities`, `last_used_at`, `expires_at`, `created_at`, `updated_at`) VALUES
(5, 'App\\Models\\User', 1, 'this.asadhashmi@gmail.com', '2cec8fe0473353ee690a2be3d9ac9b52a7f344f69041de5f89a1b84a0dcf700c', '[\"*\"]', '2023-06-22 20:37:20', NULL, '2023-06-22 20:35:51', '2023-06-22 20:37:20'),
(6, 'App\\Models\\User', 1, 'this.asadhashmi@gmail.com', '0e0154d5a29834c9d78802a96f3fa1cc4493a25e250f92b26a12ee8b29a5e0c0', '[\"*\"]', '2023-06-22 20:38:27', NULL, '2023-06-22 20:37:56', '2023-06-22 20:38:27'),
(7, 'App\\Models\\User', 1, 'this.asadhashmi@gmail.com', 'fc8d82718d2743822cbccef5837f9897e45d4913f1d14e0e3246df5c552d6dda', '[\"*\"]', '2023-06-22 20:42:12', NULL, '2023-06-22 20:38:32', '2023-06-22 20:42:12'),
(16, 'App\\Models\\User', 2, 'this.asadhashmi+1@gmail.com', 'd08d09975d5ff739d62805e35f58c3d128988a2b197111f99cfe6244d7f514ac', '[\"*\"]', '2023-06-30 15:12:57', NULL, '2023-06-26 19:43:11', '2023-06-30 15:12:57'),
(18, 'App\\Models\\User', 3, 'asad.hashmi+nano@naseebnetworks.com', '4041ddb60098d37f7afcf0f8cedab5868b15888c31478eae8f43fd3334357ed8', '[\"*\"]', '2023-06-30 15:27:14', NULL, '2023-06-30 12:50:08', '2023-06-30 15:27:14');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int NOT NULL,
  `sourceId` varchar(255) NOT NULL,
  `sourceName` varchar(255) NOT NULL,
  `author` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `description` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `url` varchar(255) NOT NULL,
  `imageURL` varchar(255) NOT NULL,
  `publishedAt` varchar(255) NOT NULL,
  `addedOn` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `sourceId`, `sourceName`, `author`, `title`, `description`, `url`, `imageURL`, `publishedAt`, `addedOn`) VALUES
(52, 'techcrunch', 'TechCrunch', 'Connie Loizos', 'Kindred Ventures foresees a \'massive explosion of startups\' courtesy of AI', 'Last week, we talked with Kindred Ventures, a small, nine-year-old, San Francisco-based early stage venture firm that, despite investing in a lot of nascent startups — more than 100 to date — takes a generalist approach, investing in AI, climate tech, consume…', 'https://techcrunch.com/2023/06/26/kindred-ventures-foresees-a-massive-explosion-of-startups-courtesy-of-ai/', 'https://techcrunch.com/wp-content/uploads/2023/06/Screen-Shot-2023-06-26-at-11.07.59-AM.png?resize=1200,818', '2023-06-26T23:03:00Z', '2023-06-28 00:33:27'),
(53, 'techcrunch', 'TechCrunch', 'Aisha Malik', 'Decentralized social networking app Damus to be removed from App Store, will appeal decision', 'Damus, a decentralized social networking app backed by Twitter co-founder Jack Dorsey, is being removed from the App Store due to Apple\'s strict payment rules.', 'https://techcrunch.com/2023/06/26/decentralized-social-networking-app-damus-to-be-removed-from-app-store-will-appeal-decision/', 'https://techcrunch.com/wp-content/uploads/2023/02/damus.jpg?resize=1200,810', '2023-06-26T21:44:58Z', '2023-06-28 00:33:27'),
(54, 'techcrunch', 'TechCrunch', 'Dr. Heather Doshay', 'SignalFire\'s State of Talent report 2023', 'Tech has seen nonstop layoffs that hit 166,044 workers in Q1 2023 alone. That’s more than all of 2022’s then-record 161,411 tech layoffs.', 'https://techcrunch.com/2023/06/26/signalfires-state-of-talent-report-2023/', 'https://techcrunch.com/wp-content/uploads/2023/06/GettyImages-182894992.jpg?resize=1200,800', '2023-06-26T20:54:12Z', '2023-06-28 00:33:27'),
(55, 'techcrunch', 'TechCrunch', 'Amanda Silberling', 'Unicorn social app IRL to shut down after admitting 95% of its users were fake', 'An internal investigation by IRL\'s board of directors found that 95% of the app\'s reported 20 million users were \"automated or from bots.\"', 'https://techcrunch.com/2023/06/26/irl-shut-down-fake-users/', 'https://techcrunch.com/wp-content/uploads/2022/06/irl-GettyImages-1152851737.jpg?resize=1200,675', '2023-06-26T20:48:18Z', '2023-06-28 00:33:27'),
(56, 'techcrunch', 'TechCrunch', 'Aisha Malik', 'Telegram is adding Stories next month', 'Telegram CEO Pavel Durov announced today that the messaging app is adding Stories in early July.', 'https://techcrunch.com/2023/06/26/telegram-is-adding-stories-next-month/', 'https://techcrunch.com/wp-content/uploads/2021/01/GettyImages-503572806.jpg?resize=1200,854', '2023-06-26T20:44:48Z', '2023-06-28 00:33:27'),
(57, 'techcrunch', 'TechCrunch', 'Kyle Wiggers', 'DeepMind claims its next chatbot will rival ChatGPT', 'In an interview with Wired, DeepMind CEO Demis Hassabis made the claim that DeepMind\'s next chatbot will perform better than OpenAI\'s ChatGPT.', 'https://techcrunch.com/2023/06/26/deepmind-claims-its-next-chatbot-will-rival-chatgpt/', 'https://techcrunch.com/wp-content/uploads/2023/05/google-io-2023-google-deepmind.jpg?resize=1200,675', '2023-06-26T19:53:12Z', '2023-06-28 00:33:27'),
(58, 'techcrunch', 'TechCrunch', 'Devin Coldewey', 'Lilz brings its gauge-watching smart cameras to the US and raises $4M', 'Lilz lets big industrial operations save time and money by automating the reading of dials and gauges hitherto only checkable by humans.', 'https://techcrunch.com/2023/06/26/lilz-brings-its-gauge-watching-smart-cameras-to-the-us-and-raises-4m/', 'https://techcrunch.com/wp-content/uploads/2023/06/lilz-devices.jpg?w=1193', '2023-06-26T19:14:06Z', '2023-06-28 00:33:27'),
(59, 'techcrunch', 'TechCrunch', 'Jacquelyn Melinek', 'Coinbase execs: As global crypto policy grows, U.S. has urgent need for legislation', '52% of companies surveyed say they\'re holding off on major crypto investments until regulation is established, according to a Coinbase and The Block report.', 'https://techcrunch.com/2023/06/26/coinbase-global-crypto-policy-urgency/', 'https://techcrunch.com/wp-content/uploads/2023/03/us-japan-south-korea-flags.jpg?resize=1200,800', '2023-06-26T18:17:38Z', '2023-06-28 00:33:27'),
(60, 'techcrunch', 'TechCrunch', 'Haje Jan Kamps', 'Russian hacking device on track to make $80 million worth of sales', 'Flipper Zero is a \"portable gamified multi-tool\" for anyone with an interest in cybersecurity, whether as a penetration tester, curious nerd or student, or with more nefarious purposes.', 'https://techcrunch.com/2023/06/26/flipper-sales/', 'https://techcrunch.com/wp-content/uploads/2023/06/20230626-IMG_5283.jpg?resize=1200,675', '2023-06-26T17:57:37Z', '2023-06-28 00:33:27'),
(61, 'techcrunch', 'TechCrunch', 'Aria Alamalhodaei', 'Max Q: Welcome to the era of MaaS (microgravity-as-a-service)', 'TechCrunch\'s weekly newsletter dedicated to all things space, including launch, satellites, space stations and more.', 'https://techcrunch.com/2023/06/26/max-q-welcome-to-the-era-of-maas-microgravity-as-a-service/', 'https://techcrunch.com/wp-content/uploads/2019/12/tc-space-stars.gif?w=946', '2023-06-23T19:08:12Z', '2023-06-28 00:33:27'),
(62, 'law', 'Guardian News', 'Law', '‘Victims are terrified’: supreme court ruling on stalking cases sparks alarm', '‘Victims are terrified’: supreme court ruling on stalking cases sparks alarm', 'https://www.theguardian.com/law/2023/jun/27/supreme-court-stalking-ruling-alarms-advocates-and-victims', 'NULL', '2023-06-28T00:21:34Z', '2023-06-28 00:33:29'),
(63, 'world', 'Guardian News', 'World news', 'Russia-Ukraine war at a glance: what we know on day 490 of the invasion', 'Russia-Ukraine war at a glance: what we know on day 490 of the invasion', 'https://www.theguardian.com/world/2023/jun/28/russia-ukraine-war-at-a-glance-what-we-know-on-day-490-of-the-invasion', 'NULL', '2023-06-28T00:18:17Z', '2023-06-28 00:33:29'),
(64, 'music', 'Guardian News', 'Music', 'Australia’s Taylor Swift fans scramble for tickets as Ticketek expects 10 times the rush of first release', 'Australia’s Taylor Swift fans scramble for tickets as Ticketek expects 10 times the rush of first release', 'https://www.theguardian.com/music/2023/jun/28/taylor-swift-presale-tickets-australia-fans-ticketek-website', 'NULL', '2023-06-27T23:26:57Z', '2023-06-28 00:33:29'),
(65, 'us-news', 'Guardian News', 'US news', 'McCarthy says Trump ‘stronger today than in 2016’ after doubting his ability to win earlier – as it happened', 'McCarthy says Trump ‘stronger today than in 2016’ after doubting his ability to win earlier – as it happened', 'https://www.theguardian.com/us-news/live/2023/jun/27/supreme-court-rulings-affirmative-action-lgbtq-rights-biden-trump-latest', 'NULL', '2023-06-27T23:05:44Z', '2023-06-28 00:33:29'),
(66, 'environment', 'Guardian News', 'Environment', 'Time is short for Tories and Labour to show leadership on the climate crisis', 'Time is short for Tories and Labour to show leadership on the climate crisis', 'https://www.theguardian.com/environment/2023/jun/28/time-is-short-for-tories-and-labour-to-show-leadership-on-the-climate-crisis', 'NULL', '2023-06-27T23:01:49Z', '2023-06-28 00:33:29'),
(67, 'media', 'Guardian News', 'Media', 'Advertising watchdog bans Hyundai and Toyota electric car ads', 'Advertising watchdog bans Hyundai and Toyota electric car ads', 'https://www.theguardian.com/media/2023/jun/28/advertising-watchdog-bans-hyundai-and-toyota-electric-car-ads', 'NULL', '2023-06-27T23:01:49Z', '2023-06-28 00:33:29'),
(68, 'money', 'Guardian News', 'Money', 'High mortgage rates forcing sellers to accept lower offers on homes – Zoopla', 'High mortgage rates forcing sellers to accept lower offers on homes – Zoopla', 'https://www.theguardian.com/money/2023/jun/28/high-mortgage-rates-forcing-sellers-to-accept-lower-offers-on-homes-zoopla', 'NULL', '2023-06-27T23:01:49Z', '2023-06-28 00:33:29'),
(69, 'environment', 'Guardian News', 'Environment', 'High costs deterring UK legal challenges to protect environment, NGOs say', 'High costs deterring UK legal challenges to protect environment, NGOs say', 'https://www.theguardian.com/environment/2023/jun/28/high-costs-deterring-uk-legal-challenges-to-protect-environment-ngos-say', 'NULL', '2023-06-27T23:01:48Z', '2023-06-28 00:33:29'),
(70, 'technology', 'Guardian News', 'Technology', 'UK has made ‘no progress’ on climate plan, say government’s own advisers', 'UK has made ‘no progress’ on climate plan, say government’s own advisers', 'https://www.theguardian.com/technology/2023/jun/28/uk-has-made-no-progress-on-climate-plan-say-governments-own-advisers', 'NULL', '2023-06-27T23:01:48Z', '2023-06-28 00:33:29'),
(71, 'tv-and-radio', 'Guardian News', 'Television & radio', 'Kanye West accused of antisemitic abuse by former business partner', 'Kanye West accused of antisemitic abuse by former business partner', 'https://www.theguardian.com/tv-and-radio/2023/jun/28/kanye-west-accused-antisemitically-abusing-former-business-partner', 'NULL', '2023-06-27T23:01:48Z', '2023-06-28 00:33:29'),
(72, '100000008969921', 'New York Times', 'By Paul Sonne and Anatoly Kurmanaev', 'His Glory Fading, a Russian Warlord Took One Last Stab at Power', 'Whatever the Wagner uprising says about Vladimir Putin’s hold on the Kremlin, it is also the story of a mercurial mercenary leader’s growing desperation.', 'https://www.nytimes.com/2023/06/27/world/europe/prigozhin-wagner-russia-putin.html', 'https://static01.nyt.com/images/2023/06/26/multimedia/26prigozhin-reconstruct-01-ptjf/26prigozhin-reconstruct-01-ptjf-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(73, '100000008968009', 'New York Times', 'By Remy Tumin', 'Uber Rider Who Killed Driver Said She Thought She Was Being Kidnapped', 'The passenger, who is charged with murder, told the authorities that she shot Daniel Piedra Garcia in the head as she worried he was taking her across the border to Mexico instead of to an El Paso casino.', 'https://www.nytimes.com/2023/06/26/us/uber-shooting-texas-mexico.html', 'https://static01.nyt.com/images/2023/06/26/us/26xp-uber1/26xp-uber1-thumbStandard.jpg', '2023-06-26', '2023-06-28 00:33:32'),
(74, '100000008970054', 'New York Times', 'By Jacey Fortin and Mary Beth Gahan', 'Teen and Stepfather Die on Hike in Near-Record Texas Heat', 'The high-pressure “heat dome” that has engulfed Texas and Oklahoma for several days is forecast to shift eastward this week, bringing dangerous temperatures to the Gulf States.', 'https://www.nytimes.com/2023/06/26/us/texas-heat-hiker-deaths.html', 'https://static01.nyt.com/images/2023/06/26/us/26nat-heat-texas/26nat-heat-texas-thumbStandard.jpg', '2023-06-26', '2023-06-28 00:33:32'),
(75, '100000008960000', 'New York Times', 'By Matt Flegenheimer and Jeremy W. Peters', 'How Fox News (Yes, Fox News) Managed to Beat ‘The Tonight Show’', 'Greg Gutfeld has installed his brand of insult conservatism as the institutional voice for the next generation of Fox News viewer. And it’s catching on.', 'https://www.nytimes.com/2023/06/27/business/greg-gutfeld-fox-news.html', 'https://static01.nyt.com/images/2023/07/02/multimedia/00Gutfeld-03-wfvz/00Gutfeld-03-wfvz-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(76, '100000008971651', 'New York Times', 'By John Koblin', 'Ryan Seacrest Named New ‘Wheel of Fortune’ Host', 'The game show has demonstrated remarkable durability even as traditional television has declined in the wake of streaming entertainment.', 'https://www.nytimes.com/2023/06/27/business/media/ryan-seacrest-wheel-of-fortune.html', 'https://static01.nyt.com/images/2023/06/28/multimedia/27wheel-B1/27wheel-zthf-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(77, '100000008970936', 'New York Times', 'By Alan Feuer', 'Judge Denies Request to Seal Witness List in Trump Documents Case', 'The order by Judge Aileen Cannon means the identities of some or all of the Justice Department’s 84 potential witnesses in the case against the former president could become public.', 'https://www.nytimes.com/2023/06/26/us/politics/trump-witness-list.html', 'https://static01.nyt.com/images/2023/06/26/multimedia/26dc-trump-witness-qbwv/26dc-trump-witness-qbwv-thumbStandard.jpg', '2023-06-26', '2023-06-28 00:33:32'),
(78, '100000008953267', 'New York Times', 'By Rochelle P. Walensky', 'What I Need to Tell America Before I Leave the C.D.C.', 'The departing C.D.C. director shares a message for Americans.', 'https://www.nytimes.com/2023/06/27/opinion/rochelle-walensky-cdc-pandemic-despair.html', 'https://static01.nyt.com/images/2023/06/27/opinion/27Walensky/27Walensky-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(79, '100000008971968', 'New York Times', 'By Julie Bosman', 'Smoky Air From Canadian Wildfires Blankets Midwestern Skies', 'Chicago residents were warned to stay indoors or wear masks, and the popular paths along Lake Michigan were quiet.', 'https://www.nytimes.com/2023/06/27/us/midwest-chicago-smoke-air-quality.html', 'https://static01.nyt.com/images/2023/06/27/multimedia/NAT-MIDWEST-SMOKE-1-kbgv/NAT-MIDWEST-SMOKE-1-kbgv-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(80, '100000007781114', 'New York Times', 'By Tanya Sichynsky', 'Fourth of July Side Dishes That Will Steal the Show', 'Wow your friends and family with coleslaws, potato salad and other summer favorites that don’t shy away from big flavor.', 'https://www.nytimes.com/article/best-barbecue-side-dishes.html', 'https://static01.nyt.com/images/2020/07/30/dining/25barbecuesides-top-art/mp-mango-slaw-thumbStandard.jpg', '2021-05-26', '2023-06-28 00:33:32'),
(81, '100000008969050', 'New York Times', 'By Salvatore Mercogliano', 'The Story of the Titan Submersible Has Not Ended', 'The loss of the Titan must lead to better safety, just as the loss of the Titanic did.', 'https://www.nytimes.com/2023/06/27/opinion/oceangate-submersible-titan-tragedy.html', 'https://static01.nyt.com/images/2023/06/28/multimedia/27Mercogliano-vqcm/27Mercogliano-vqcm-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(82, '100000008956638', 'New York Times', 'By Carla Sosenko', 'New Tours Mean No More FOMO for Plus-Size Travelers', 'Five companies dedicated to size-inclusive travel aim to bring community and reassurance to people in bigger bodies.', 'https://www.nytimes.com/2023/06/27/travel/size-inclusive-tours.html', 'https://static01.nyt.com/images/2023/06/21/travel/oakImage-1687356498576/oakImage-1687356498576-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(83, '100000008965253', 'New York Times', 'By Genevieve Glatsky and Julie Turkewitz', 'The Unexpected Rescuers Who Found Colombia’s Missing Children', 'Colombia’s Indigenous Guard has long had to fight for a space in the national narrative. Today, it is at the center of the country’s biggest story.', 'https://www.nytimes.com/2023/06/27/world/americas/children-rescue-plane-crash-colombia.html', 'https://static01.nyt.com/images/2023/06/25/world/25colombia-children/25colombia-children-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(84, '100000008924251', 'New York Times', 'By Catherine Pearson', 'Life in the Throes of Postpartum Depression', 'Four new mothers open up about the common yet isolating struggle.', 'https://www.nytimes.com/2023/06/27/well/mind/postpartum-depression-mental-health.html', 'https://static01.nyt.com/images/2023/06/14/multimedia/14WELL-POSTPARTUM-ENTERPRISE-bcmf/14WELL-POSTPARTUM-ENTERPRISE-bcmf-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(85, '100000008971570', 'New York Times', 'By German Lopez', '115 Degrees Fahrenheit', 'Summer technically just began, and parts of the U.S. already have dangerous heat.', 'https://www.nytimes.com/2023/06/27/briefing/weather-heat-temperatures.html', 'https://static01.nyt.com/images/2023/06/27/multimedia/27-the-morning-heat-promo/27-the-morning-heat-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(86, '100000008962667', 'New York Times', 'By Jack Ewing', 'Tesla May Have Already Won the Charging Wars', 'Deals with Ford and G.M. will make it easier to find a charger but could give Elon Musk control of critical infrastructure.', 'https://www.nytimes.com/2023/06/27/business/energy-environment/tesla-gm-ford-charging-electric-vehicles.html', 'https://static01.nyt.com/images/2023/06/27/multimedia/00tesla-charging1-print-jgbz/00tesla-charging-03-jgbz-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(87, '100000008954697', 'New York Times', 'By Kate Guadagnino', 'A New Line of Subtle, Glamorous Clothes', 'The debut collection from the designer Michelle Rhee’s namesake line balances romantic draping with precise men’s wear-inspired silhouettes.', 'https://www.nytimes.com/2023/06/23/t-magazine/michelle-rhee-fashion.html', 'https://static01.nyt.com/images/2023/06/21/t-magazine/fashion/21tmag-michelle-rhee-slide-6CT4-copy/21tmag-michelle-rhee-slide-6CT4-copy-thumbStandard.jpg', '2023-06-23', '2023-06-28 00:33:32'),
(88, '100000008929351', 'New York Times', 'By Adam Liptak', 'Supreme Court Rejects Theory That Would Have Transformed American Elections', 'The 6-to-3 majority dismissed the “independent state legislature” theory, which would have given state lawmakers nearly unchecked power over federal elections.', 'https://www.nytimes.com/2023/06/27/us/politics/supreme-court-state-legislature-elections.html', 'https://static01.nyt.com/images/2023/06/27/multimedia/27dc-scotus-elections-01-gkjz/27dc-scotus-elections-01-gkjz-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(89, '100000008963725', 'New York Times', 'By Mihir Zaveri', 'Why Mark Ruffalo and Wendell Pierce Are Fighting for a Crumbling Church', 'Congregants of the West Park Presbyterian Church, a Manhattan landmark, want it torn down and replaced by condos. Celebrities are joining the fight to save it.', 'https://www.nytimes.com/2023/06/26/nyregion/west-park-presbyterian-church-manhattan.html', 'https://static01.nyt.com/images/2023/06/26/multimedia/26-NY-LANDMARK-PRINT/26ny-landmark-zjbl-thumbStandard.jpg', '2023-06-26', '2023-06-28 00:33:32'),
(90, '100000008950769', 'New York Times', 'By Melinda Wenner Moyer', 'Why Do I Wake Up Right Before My Alarm?', 'Experts have some theories.', 'https://www.nytimes.com/2023/06/27/well/live/wake-up-before-alarm-clock.html', 'https://static01.nyt.com/images/2023/06/27/multimedia/27ASKWELL-WAKEUP-2-tjpv/27ASKWELL-WAKEUP-2-tjpv-thumbStandard.jpg', '2023-06-27', '2023-06-28 00:33:32'),
(91, '100000008435335', 'New York Times', 'By Matthew Bloch, Lazaro Gamio, Zach Levitt, Eleanor Lutz, Bea Malsky and John-Michael Murphy', 'Tracking Dangerous Heat in the U.S.', 'See detailed maps of the latest heat index forecasts in the United States.', 'https://www.nytimes.com/interactive/2022/us/heat-wave-map-tracker.html', 'https://static01.nyt.com/images/2022/07/11/us/heat-wave-map-tracker-promo/heat-wave-map-tracker-promo-thumbStandard-v410.png', '2022-07-12', '2023-06-28 00:33:32');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tc` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `tc`, `created_at`, `updated_at`) VALUES
(1, 'Asad', 'this.asadhashmi@gmail.com', NULL, '$2y$10$uPuSAtGTxzWgcdm5/PpoGusJKzTZSVDauqdqSuqcwGDBDqP11eZbO', NULL, 1, '2023-06-22 20:25:59', '2023-06-22 20:42:12'),
(2, 'Asad  Hussain Hashmi', 'this.asadhashmi+1@gmail.com', NULL, '$2y$10$JLqctUd77nmHEP9RJATVje4bZkEnZiFCUihsrzvuCZnxqSwBBbz4a', NULL, 1, '2023-06-23 18:03:51', '2023-06-25 20:20:29'),
(3, 'Asad Hashmi', 'asad.hashmi+nano@naseebnetworks.com', NULL, '$2y$10$qqspCEuFYwggYY6qjxwiSuHmwBCa1jxfG8PuXJKvdvUwDHwudHRA6', NULL, 1, '2023-06-25 20:29:20', '2023-06-25 20:29:20');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `apiLogs`
--
ALTER TABLE `apiLogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_reset_tokens`
--
ALTER TABLE `password_reset_tokens`
  ADD PRIMARY KEY (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `apiLogs`
--
ALTER TABLE `apiLogs`
  MODIFY `id` int NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
