<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use DB;
class NewsApiController extends Controller
{

    public function getNewsApiData()
    {
        $loggeduser = auth()->user();
        $newsData = [];
            
        // $this->getDataFromCloudApis();  // getData from apis 
        $newsData=$this->getNewsDataViaDb();

         return response([
            'apiData' => $newsData,
            'message' => 'NewsApi Data Served',
            'status' => 'success'
        ], 200);
    }


    public function getDataFromCloudApis()
    {
       
        // $newsData = $this->fetchingDataFromSources($params = []);  fetching Data using Multi curl , but its giving me errors and have no time left to debug so i am using seperat calls for now

        $newsData = $this->getNewsApiProcessedData($params = []); // news Api Data 
        $newsData = $this->getGuardianApiProcessedData($params = []); // Guardian Api Data
        $newsData = $this->getNYTimesApiProcessedData($params = []); // Guardian Api Data
        
      return true;
    }

    public function getNewsDataViaDb($params=[]){
       $newsDataDb = DB::select('select * from  posts');
        return $newsDataDb;
    } 


    public function getNewsApiProcessedData($params = [])
    {

        $curl = curl_init();
        $agent = 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)';

        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => 'https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=57dbe6eb41cd4688952833475dc0cc0b',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Authorization: 57dbe6eb41cd4688952833475dc0cc0b',
                    'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
                    'Accept: application/json'
                ),
            )
        );

        $response = curl_exec($curl);
        curl_close($curl);

        $newsApiData=json_decode($response,true);

        foreach($newsApiData['articles'] as $key=>$val){
        
              $sourceid=$val['source']['id']; 
              $sourceName=$val['source']['name']; 
              $author=$val['author']; 
              $title=$val['title']; 
              $url=$val['url'];
              $description=$val['description']; 
              $image=$val['urlToImage']; 
              $publishDate=$val['publishedAt']; 

            //   DB::insert('insert into posts (id, name) values (?, ?)', [1, 'Dayle']);
              $data=array('sourceId'=>$sourceid,"sourceName"=>$sourceName,"author"=>$author,"title"=>$title,"description"=>$description,"url"=>$url,"imageURL"=>$image,"publishedAt"=>$publishDate);
              DB::table('posts')->insert($data);

        }
        

        return true;
    }

    public function getGuardianApiProcessedData($params = [])
    {


        $curl = curl_init();

        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => 'https://content.guardianapis.com/search?api-key=b6031c65-0e2a-4b6c-9abc-a5db85cfa8da',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'Cookie: AWSALB=umwUu7ktDoJOFIAhEZZYZBIB5mRKdtUTCOxrukmXBX9ix3IOnzHYPFJ2Baxpw3UibHEGl3cTRrddSPr4R/aaxC3IJBQ/DcTnCJqLxt8/I5ZdS1J0MUPby3wW7byY; AWSALBCORS=umwUu7ktDoJOFIAhEZZYZBIB5mRKdtUTCOxrukmXBX9ix3IOnzHYPFJ2Baxpw3UibHEGl3cTRrddSPr4R/aaxC3IJBQ/DcTnCJqLxt8/I5ZdS1J0MUPby3wW7byY',
                    'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
                    'Accept: application/json'
                ),
            )
        );

        $response = curl_exec($curl);
        curl_close($curl);

        $guardianApiData=json_decode($response,true);

        foreach($guardianApiData['response']['results'] as $key=>$val){
            
            
              $sourceid=$val['sectionId']; 
              $sourceName='Guardian News'; 
              $publishDate=$val['webPublicationDate']; 
              $author=$val['sectionName']; 
              $title=$val['webTitle']; 
              $url=$val['webUrl'];
              $description=$val['webTitle']; 
              $image='NULL'; 
              

            //   DB::insert('insert into posts (id, name) values (?, ?)', [1, 'Dayle']);
              $data=array('sourceId'=>$sourceid,"sourceName"=>$sourceName,"author"=>$author,"title"=>$title,"description"=>$description,"url"=>$url,"imageURL"=>$image,"publishedAt"=>$publishDate);
              DB::table('posts')->insert($data);

        }
        

        return true;
    }
    public function getNYTimesApiProcessedData($params = [])
    {


        $curl = curl_init();

        curl_setopt_array(
            $curl,
            array(
                CURLOPT_URL => 'https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=RhGAJXfShffEO1zeTsPpN3ltBx8EiLDY',
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => '',
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => 'GET',
                CURLOPT_HTTPHEADER => array(
                    'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
                    'Accept: application/json'
                ),
            )
        );

        $response = curl_exec($curl);
        curl_close($curl);

        $nytData=json_decode($response,true);

        foreach($nytData['results'] as $key=>$val){
              $sourceid=$val['id']; 
              $sourceName=$val['source']; 
              $author=$val['byline']; 
              $title=$val['title']; 
              $url=$val['url'];
              $description=$val['abstract']; 
              $image=$val['media'][0]['media-metadata'][0]['url']; 
              $publishDate=$val['published_date']; 

            //   DB::insert('insert into posts (id, name) values (?, ?)', [1, 'Dayle']);
              $data=array('sourceId'=>$sourceid,"sourceName"=>$sourceName,"author"=>$author,"title"=>$title,"description"=>$description,"url"=>$url,"imageURL"=>$image,"publishedAt"=>$publishDate);
              DB::table('posts')->insert($data);

        }exit();
        

        return true;
    }

    public function fetchingDataFromSources($params = [])   // using multi curl , its giving me errors at the last moment so cant use it right now , will have to call them seperatly
    {

        $request_contents = array();
        $urls = array();
        $chs = array();


        //set the urls
        $urls['nyt'] = 'https://api.nytimes.com/svc/mostpopular/v2/viewed/1.json?api-key=RhGAJXfShffEO1zeTsPpN3ltBx8EiLDY';
        $urls['guardian'] = 'https://content.guardianapis.com/search?api-key=b6031c65-0e2a-4b6c-9abc-a5db85cfa8da';
        $urls['newsApi'] = 'https://newsapi.org/v2/top-headlines?sources=techcrunch&apiKey=57dbe6eb41cd4688952833475dc0cc0b';

       

        $httpHeaderArray = array(
            'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
            'Accept: application/json'
        );

        $httpHeaderArrayForGuardian = array(
            'Cookie: AWSALB=umwUu7ktDoJOFIAhEZZYZBIB5mRKdtUTCOxrukmXBX9ix3IOnzHYPFJ2Baxpw3UibHEGl3cTRrddSPr4R/aaxC3IJBQ/DcTnCJqLxt8/I5ZdS1J0MUPby3wW7byY; AWSALBCORS=umwUu7ktDoJOFIAhEZZYZBIB5mRKdtUTCOxrukmXBX9ix3IOnzHYPFJ2Baxpw3UibHEGl3cTRrddSPr4R/aaxC3IJBQ/DcTnCJqLxt8/I5ZdS1J0MUPby3wW7byY',
            'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
            'Accept: application/json'
        );
        $httpHeaderArrayForNewsApi = array(
            'Authorization: 57dbe6eb41cd4688952833475dc0cc0b',
            'User-Agent: Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
            'Accept: application/json'
        );

        $mh = curl_multi_init();
        foreach ($urls as $key => $url) {
            
            
            $chs[$key] = curl_init($url);
            curl_setopt($chs[$key], CURLOPT_RETURNTRANSFER, true);

            curl_setopt($chs[$key], CURLOPT_RETURNTRANSFER, true);
            curl_setopt($chs[$key], CURLOPT_ENCODING, '');
            curl_setopt($chs[$key], CURLOPT_MAXREDIRS, 10);
            curl_setopt($chs[$key], CURLOPT_TIMEOUT, 0);
            curl_setopt($chs[$key], CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($chs[$key], CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_1);
            curl_setopt($chs[$key], CURLOPT_CUSTOMREQUEST, 'GET');

            if ($key == 'guardian') {
                curl_setopt($chs[$key], CURLOPT_HTTPHEADER, $httpHeaderArrayForGuardian);

            } elseif ($key == 'newsApi') {
                curl_setopt($chs[$key], CURLOPT_HTTPHEADER, $httpHeaderArrayForNewsApi);

            } else {
                curl_setopt($chs[$key], CURLOPT_HTTPHEADER, $httpHeaderArray);

            }

            curl_setopt($chs[$key], CURLOPT_POST, true);
            // curl_setopt($chs[$key], CURLOPT_POSTFIELDS, $request_contents[$key]);


            curl_multi_add_handle($mh, $chs[$key]);
        }
        

        //running the requests
        $running = null;
        do {
            curl_multi_exec($mh, $running);
        } while ($running);

        //getting the responses
        // $response='';
        foreach (array_keys($chs) as $key) {
            $error = curl_error($chs[$key]);
            $last_effective_URL = curl_getinfo($chs[$key], CURLINFO_EFFECTIVE_URL);
            $time = curl_getinfo($chs[$key], CURLINFO_TOTAL_TIME);
            $response = curl_multi_getcontent($chs[$key]); // get results
           /* if (!empty($error)) {
                echo "The request $key return a error: $error" . "\n";
            } else {
                echo "The request to '$last_effective_URL' returned '$response' in $time seconds." . "\n";
            }*/

            curl_multi_remove_handle($mh, $chs[$key]);
        }

        // close current handler
        curl_multi_close($mh);

        echo $response;
       


    }










}